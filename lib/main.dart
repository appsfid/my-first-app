import 'package:flutter/material.dart';

/*
 * 📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱
 *                 Flutter
 *         Creating Your First App
 * 📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱📱 
 */

// - [v] Flutter Installation & Creating Your First App
// - [v] Your First App - Object Oriented!
// - [v] Your First App - Git & GitLab!
// - [v] Your First App - Add botton & Callback!

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.light(),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage() {}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My First App"),
      ),
      body: Center(
        child: RaisedButton(
          child: Text("Click!"),
          onPressed: () {
            print("the button is clicked!");
          },
        ),
      ),
    );
  }
}
